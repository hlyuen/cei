$(function(){
	initMenu();
	console.log($(window).width());
});
function initMenu(){
	$('.menuWrapper .lv1Menu').click(function(event){
		event.stopPropagation();
		$('.menuWrapper .lv1Menu').removeClass("act");
		$(this).addClass("act");
	});
	$('*').not('.lv1Menu').click(function(){
		$('.menuWrapper .lv1Menu').removeClass("act");
	});	
	$('.menuBtn a.menuText').click(function(){
		$('.menuWrapper').toggleClass("act");
	});
	$('.menuClose').click(function(){
		$('.menuWrapper').removeClass("act");
	});
	$(window).scroll(function(){
		var adHeight = $('.adWrapper').height();
		var scrollTop = $(window).scrollTop();
		if(scrollTop > adHeight){
			$('header').addClass("sticky");
		}else{
			$('header').removeClass("sticky");
		}
	});
}