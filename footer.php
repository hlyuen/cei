<footer>
    <div class="row">
    	<div class="upper">
            <div class="left">
                <p class="text">Get Listed to the CEI Venue Directory:<br>Phone +852 3175 1937<br>in business hours or Online.<br> Or click:</p>
                <div class="getListedWrapper"><a href="javascript:;" class="navBtn">+ Get Listed</a></div>
            </div>
            <div class="right">
                <ul>
                    <li><a href="javascript:;">eNewsletter Archieve</a></li>
                    <li><a href="javascript:;">RSS</a></li>
                    <li><a href="javascript:;">Register for Newsletters</a></li>
                    <li><a href="javascript:;">subscribe to Magazine</a></li>
                </ul>
                <ul>
                    <li><a href="javascript:;">About</a></li>
                    <li><a href="javascript:;">Contact</a></li>
                    <li><a href="javascript:;">Advertise</a></li>
                    <li><a href="javascript:;">Magazine to Archive</a></li>
                </ul>
            </div>
		</div>
        <?php include "social.php"; ?>
        <div class="copyright">
        	<div class="logo"><img src="images/haymarket.png"></div>
        	<p>Copyright 2015 Haymarket Media. All right reserved. This material may not be published, rewritten or redistributed in any form without prior authorisation.<br>Your use of this website constitutes acceptance of Haymarket Media's <a href="javascript:;">Privacy Policy</a> and <a href="javascript:;">Term & Conditions</a></p>
        </div>
	</div>
</footer>