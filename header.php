<header>
    	<div class="row">
        	<div class="padding">
	        	<div class="logo"><img src="images/logo_spacer.png"></div>
				<div class="menuBtn">
                	<a href="javascript:;" class="menuText">Menu</a>
                	<div class="menuWrapper">
                    	<a href="javascript:;" class="menuClose">X</a>
	                    <nav class="dropDownList">
    	                    <ul>
        	                    <li class="lv1Menu">
            	                    <a href="javascript:;" class="section">Destination</a>
                	                <nav class="submenu">
                    	                <ul>
                        	                <li><a href="javascript:;">Destination News</a></li>
                            	            <li><a href="javascript:;">Destination Features</a></li>
                                	        <li><a href="javascript:;">Destination Opinion</a></li>
	                                    </ul>
    	                            </nav>
        	                    </li>
            	                <li class="lv1Menu">
                	                <a href="javascript:;" class="section">Venue</a>
                    	            <nav class="submenu">
                        	            <ul>
                            	            <li><a href="javascript:;">Venue News</a></li>
                                	        <li><a href="javascript:;">Venue Features</a></li>
                                    	    <li><a href="javascript:;">Venue Opinion</a></li>
	                                    </ul>
    	                            </nav>
        	                    </li>
            	                <li class="lv1Menu">
                	                <a href="javascript:;" class="section">Industrial & People</a>
                    	            <nav class="submenu">
                        	            <ul>
                            	            <li><a href="javascript:;">Industrial News</a></li>
                                	        <li><a href="javascript:;">Industrial Features</a></li>
                                    	    <li><a href="javascript:;">Industrial Opinion</a></li>
	                                    </ul>
    	                            </nav>
        	                    </li>
							</ul>
        	            </nav>
            	        <div class="menuBottom">
                	        <div class="linkWrapper">
                    	        <a href="javascript:;" class="link">Subscribe</a>
                        	    <a href="javascript:;" class="link">Login</a>
	                        </div>
    	                    <?php include "social.php"; ?>
        	            </div>
					</div>
				</div>
                <div class="navBtnWrapper">
                    <a href="javascript:;" class="navBtn destination show-for-medium-up">Destination Discovery</a>
                    <a href="javascript:;" class="navBtn venue show-for-medium-up">Venue Discovery</a>
                    <a href="javascript:;" class="navBtn getListed">+ Get Listed</a>
                    <div class="linkWrapper">
                        <a href="javascript:;" class="link show-for-medium-up">Subscribe</a>
                        <a href="javascript:;" class="link show-for-large-up">Login</a>
                    </div>
                </div>
            </div>
            <div class="searchWrapper">
                <section class="searchBox">
                    <form id="searchForm" action="">
                        <input placeholder="tell us what you're looking for" value="" id="search">
                        <select id="searchSelect">
                            <option value="" selected>All</option>
                            <option value="destination" selected>Destination</option>
                        </select>
                        <input placeholder="" value="Search" id="searchSubmit">
                    </form>
                </section>
				<?php include "social.php"; ?>
            </div>            
		</div>
    </header>