<?php include "html_header.php"; ?>
<body id="home">
	<?php include "ad.php"; ?>
  	<?php include "header.php"; ?>
    <section id="hero1" class="hero row">    
		<div class="intro">
        	<div class="columns text-center"> 
				<h1>You're planning your next corporate event</h1>
				<h2>use our trusted content and directories to help</h2>
			</div>
		</div>
    </section>
    <div class="mainContent">
        <div class="section">
			<h1 class="sectionTitle text-center">Latest Stories</h1>        
            <div class="grid col-2-main">
                <div class="col mainCol">
                    <div class="imgBox">
                        <a class="btn newsBtn" href="javascript:;">Venue News</a>
                        <img src="images/home_story_main.jpg">
                        <div class="overlay">
                            <div class="text">
                                <h2>Bikers flock to Bali<br>for Harley</h2>
                                <p class="date">19 June 2015</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col subCol">
					<div class="space"><img src="images/home_ad_block1.jpg"></div>
					<div><img src="images/home_ad_block2.jpg"></div>
                </div>
            </div>        
		    <div class="grid col-3">
                <div class="col">
                    <div class="imgBox">
                        <a class="btn newsBtn" href="javascript:;">Venue News</a>
                        <img src="images/home_story_img1.jpg">                        
                        <div class="overlay">
                            <div class="text">
                                <h2>Bikers flock to Bali<br>for Harley</h2>
                                <p class="date">19 June 2015</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="imgBox">
                        <a class="btn newsBtn" href="javascript:;">Venue News</a>
                        <img src="images/home_story_img1.jpg">                        
                        <div class="overlay">
                            <div class="text">
                                <h2>Bikers flock to Bali<br>for Harley</h2>
                                <p class="date">19 June 2015</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="imgBox">
                        <a class="btn newsBtn" href="javascript:;">Venue News</a>
                        <img src="images/home_story_img1.jpg">                        
                        <div class="overlay">
                            <div class="text">
                                <h2>Bikers flock to Bali<br>for Harley</h2>
                                <p class="date">19 June 2015</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>        
            <div class="buttonRow">
                <div class="large-4 columns small-centered btnWrapper"><a class="btn" href="javascript:;">All Stories</a></div></div>
            </div>            
        </div>
    </div>
    <section id="hero2" class="hero row">    
		<div class="intro">
        	<div class="columns text-center"> 
				<h1>Browse the CEI Destination & Venue Directories</h1>
				<div class="heroBtnWrapper">
                	<a class="btn destination" href="javascript:;">Destination Discovery</a><br>
                    <a class="btn venue" href="javascript:;">Venue Discoverty</a>
                </div>
			</div>
		</div>
    </section>
    <div class="mainContent">
        <div class="section greyBg">
			<h1 class="sectionTitle text-center">Sponsored Destination Listings</h1>
            <div class="grid col-3">
                <div class="col">
                    <div class="imgBox">
                        <img src="images/home_australia.jpg">
                    </div>
                </div>
                <div class="col">
                    <div class="imgBox">            
                        <img src="images/home_macau.jpg">
                    </div>
                </div>
                <div class="col">
                    <div class="imgBox">            
                        <img src="images/home_thailand.jpg">
                    </div>
                </div>
	        	<div class="large-4 columns small-centered btnWrapper"><a class="btn destination" href="javascript:;">Destination Discovery</a></div>
            </div>
            <div class="separater"></div>
			<h1 class="sectionTitle text-center">Sponsored Venue Listings</h1>
            <div class="grid col-3">
                <div class="col">
                    <div class="imgBox">
                        <img src="images/home_australia.jpg">
                    </div>
                </div>
                <div class="col">
                    <div class="imgBox">            
                        <img src="images/home_macau.jpg">
                    </div>
                </div>
                <div class="col">
                    <div class="imgBox">            
                        <img src="images/home_thailand.jpg">
                    </div>
                </div>
            </div>
			<div class="large-4 columns small-centered btnWrapper"><a class="btn venue" href="javascript:;">Venue Discovery</a></div>            
		</div>
        <div class="section">
			<h1 class="sectionTitle text-center">Latest Case Studies</h1>
            <div class="grid col-3">
                <div class="col">
                    <div class="imgBox">
                        <img src="images/home_australia.jpg">
                    </div>
                </div>
                <div class="col">
                    <div class="imgBox">            
                        <img src="images/home_macau.jpg">
                    </div>
                </div>
                <div class="col">
                    <div class="imgBox">            
                        <img src="images/home_thailand.jpg">
                    </div>
                </div>
            </div>
            <div class="grid col-3">
                <div class="col">
                    <div class="imgBox">
                        <img src="images/home_australia.jpg">
                    </div>
                </div>
                <div class="col">
                    <div class="imgBox">            
                        <img src="images/home_macau.jpg">
                    </div>
                </div>
                <div class="col">
                    <div class="imgBox">            
                        <img src="images/home_thailand.jpg">
                    </div>
                </div>
            </div>
			<div class="large-4 columns small-centered btnWrapper"><a class="btn" href="javascript:;">All Case Studies</a></div>            
		</div>
        <div class="section greyBg">
			<h1 class="sectionTitle text-center">Latest Video</h1>
            <div class="grid col-4">
                <div class="col">
                    <div class="imgBox style2">
                        <a class="btn newsBtn" href="javascript:;">Venue News</a>
                        <a href="javascript:;"><img src="images/home_video_dummy.jpg"></a>
                        <div class="overlay">
                            <div class="text">
                                <h2>Bikers flock to Bali<br>for Harley<br>for Harley</h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="imgBox style2">
                        <a class="btn newsBtn" href="javascript:;">Venue News</a>
                        <a href="javascript:;"><img src="images/home_video_dummy.jpg"></a>
                        <div class="overlay">
                            <div class="text">
                                <h2>Bikers flock to Bali<br>for Harley<br>for Harley</h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="imgBox style2">
                        <a class="btn newsBtn" href="javascript:;">Venue News</a>
                        <a href="javascript:;"><img src="images/home_video_dummy.jpg"></a>
                        <div class="overlay">
                            <div class="text">
                                <h2>Bikers flock to Bali<br>for Harley<br>for Harley</h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="imgBox style2">
                        <a class="btn newsBtn" href="javascript:;">Venue News</a>
                        <a href="javascript:;"><img src="images/home_video_dummy.jpg"></a>
                        <div class="overlay">
                            <div class="text">
                                <h2>Bikers flock to Bali<br>for Harley<br>for Harley</h2>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
            <div class="grid col-4">
                <div class="col">
                    <div class="imgBox style2">
                        <a class="btn newsBtn" href="javascript:;">Venue News</a>
                        <a href="javascript:;"><img src="images/home_video_dummy.jpg"></a>
                        <div class="overlay">
                            <div class="text">
                                <h2>Bikers flock to Bali<br>for Harley<br>for Harley</h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="imgBox style2">
                        <a class="btn newsBtn" href="javascript:;">Venue News</a>
                        <a href="javascript:;"><img src="images/home_video_dummy.jpg"></a>
                        <div class="overlay">
                            <div class="text">
                                <h2>Bikers flock to Bali<br>for Harley<br>for Harley</h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="imgBox style2">
                        <a class="btn newsBtn" href="javascript:;">Venue News</a>
                        <a href="javascript:;"><img src="images/home_video_dummy.jpg"></a>
                        <div class="overlay">
                            <div class="text">
                                <h2>Bikers flock to Bali<br>for Harley<br>for Harley</h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="imgBox style2">
                        <a class="btn newsBtn" href="javascript:;">Venue News</a>
                        <a href="javascript:;"><img src="images/home_video_dummy.jpg"></a>
                        <div class="overlay">
                            <div class="text">
                                <h2>Bikers flock to Bali<br>for Harley<br>for Harley</h2>
                            </div>
                        </div>
                    </div>
                </div>
			</div>            
			<div class="large-4 columns small-centered btnWrapper"><a class="btn" href="javascript:;">All Video</a></div>
		</div>        
        <div class="section">
			<h1 class="sectionTitle text-center">Don't Miss</h1>
            <div class="grid col-4">
                <div class="col">
                    <div class="imgBox style3">
                        <a class="btn newsBtn" href="javascript:;">Venue News</a>
                        <a href="javascript:;"><img src="images/home_video_dummy.jpg"></a>
                        <div class="overlay">
                            <div class="text">
		                        <span class="num">1</span>
                                <h2>Bikers flock to Bali<br>for Harley<br>for Harley</h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="imgBox style3">
                        <a class="btn newsBtn" href="javascript:;">Venue News</a>
                        <a href="javascript:;"><img src="images/home_video_dummy.jpg"></a>
                        <div class="overlay">
                            <div class="text">
                            	<span class="num">2</span>
                                <h2>Bikers flock to Bali<br>for Harley<br>for Harley</h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="imgBox style3">
                        <a class="btn newsBtn" href="javascript:;">Venue News</a>
                        <a href="javascript:;"><img src="images/home_video_dummy.jpg"></a>
                        <div class="overlay">
                            <div class="text">
                            	<span class="num">3</span>
                                <h2>Bikers flock to Bali<br>for Harley<br>for Harley</h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="imgBox style3">
                        <a class="btn newsBtn" href="javascript:;">Venue News</a>
                        <a href="javascript:;"><img src="images/home_video_dummy.jpg"></a>
                        <div class="overlay">
                            <div class="text">
                            	<span class="num">4</span>
                                <h2>Bikers flock to Bali<br>for Harley<br>for Harley</h2>
                            </div>
                        </div>
                    </div>
                </div>
			</div>            
        </div>    
        <div class="section greyBg" id="issue">
			<h1 class="sectionTitle text-center">Current Issue</h1>
            <div class="grid col-2">
            	<div class="col">
                	<div class="book"><img src="images/home_issue_book.jpg"></div>
                    <div class="text">
                    	<h2>May 2015 Issue</h2>
                        <p>Summary Senior appointments at China hotels<br>Summary Senior appointments at China hotels<br>Summary Senior appointments at China hotels<br>Summary Senior appointments at China hotels</p>
                        <div class="btnWrapper"><a class="btn" href="javascript:;">Available Now</a></div>
                    </div>
                    <div class="separater"></div>
                </div>
            	<div class="col">
                	<img src="images/home_ad_block2.jpg">
                </div>
            </div>
		</div>
        <div class="section">
			<h1 class="sectionTitle text-center">Sponsored Supplements</h1>
            <div class="grid col-2-even">
            	<div class="col">
					<div class="cover"><img src="images/home_supplements_dummy.jpg"></div>
					<div class="cover"><img src="images/home_supplements_dummy.jpg"></div>                    
                </div>
            	<div class="col">
					<div class="cover"><img src="images/home_supplements_dummy.jpg"></div>
					<div class="cover"><img src="images/home_supplements_dummy.jpg"></div>                    
                </div>
            </div>
	</div>
	<?php include "footer.php"; ?>
	<?php include "js.php"; ?>
</body>
</html>
